package com.thatchai.week6;

import javax.swing.text.html.HTMLDocument.HTMLReader.PreAction;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank thatchai = new BookBank("Thatchai", 100.0);
        thatchai.print();
        thatchai.deposit(50);
        thatchai.print();
        thatchai.withdraw(50);
        thatchai.print();

        BookBank prayood = new BookBank("Prayood", 1);
        prayood.deposit(1000000);
        prayood.withdraw(10000000);
        prayood.print();

        BookBank praweet = new BookBank("Praweet", 10);
        praweet.deposit(10000000);
        praweet.withdraw(1000000);
        praweet.print();
    }
}
